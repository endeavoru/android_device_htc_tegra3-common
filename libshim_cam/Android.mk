LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
slock.c

LOCAL_SHARED_LIBRARIES := liblog libcutils libgui libbinder libutils libshim_skia
LOCAL_MODULE := libshim_cam
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
slock.c

LOCAL_SHARED_LIBRARIES := liblog libcutils libgui libbinder libutils libshim_skia
LOCAL_MULTILIB := both
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
LOCAL_MODULE := libshim_cam
LOCAL_LDLIBS := -llog
LOCAL_MODULE_TAGS := optional

include $(BUILD_STATIC_LIBRARY)
